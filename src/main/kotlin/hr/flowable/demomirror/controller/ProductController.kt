package hr.flowable.demomirror.controller

import hr.flowable.demomirror.repo.ProductRepository
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
@Component
class ProductController(private val productRepository: ProductRepository) {

    @GetMapping("/")
    fun hello() = "Mirror demo is up and running..."

    @GetMapping("/products")
    fun getTestProducts() = productRepository.fetchTestProducts()

    @PostMapping(path = ["/scan/cabin/1"], consumes = ["text/plain"], produces = ["text/plain"])
    fun scanId(@RequestBody id: String) = productRepository.scanId(id)

    @GetMapping("/scanned/cabin/1")
    fun getScannedIds() = productRepository.fetchScannedIds()

    @GetMapping("/clear/cabin/1")
    fun clearScannedIds() = productRepository.clearScannedIds()
}