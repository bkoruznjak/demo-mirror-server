package hr.flowable.demomirror.repo

import hr.flowable.demomirror.model.Product
import hr.flowable.demomirror.socket.SocketHandler
import org.springframework.stereotype.Repository

@Repository
class ProductRepository {

    private val scannedIds = mutableListOf<String>()

    private val listOfDomains = listOf(
            "https://smartmirrordemo.s3.eu-central-1.amazonaws.com/"
    )

    private val similarShirts = listOf(
            Product(
                    id = "5",
                    name = "ELLESSE majica kratkih rukava MENS ITALIA",
                    description = "129.00 HRK",
                    size = "L",
                    availableSizes = listOf("L", "XL", "2XL"),
                    similarItems = null,
                    imageUrl = "${listOfDomains[0]}img_tshirt_white_alt_one.jpg"
            ),
            Product(
                    id = "6",
                    name = "ELLESSE majica kratkih rukava MENS ITALIA",
                    description = "129.00 HRK",
                    size = "L",
                    availableSizes = listOf("S", "M", "L", "XL", "2Xl"),
                    similarItems = null,
                    imageUrl = "${listOfDomains[0]}img_tshirt_white_alt_three.jpg"
            ),
            Product(
                    id = "7",
                    name = "ELLESSE majica kratkih rukava MENS HERITAGE",
                    description = "129.00 HRK",
                    size = "L",
                    availableSizes = listOf("M", "L", "XL", "2XL"),
                    similarItems = null,
                    imageUrl = "${listOfDomains[0]}img_tshirt_white_alt_two.jpg"
            )
    )

    private val similarShorts = listOf(
            Product(
                    id = "8",
                    name = "ADIDAS kratke hlače REAL H SHO",
                    description = "239.20 HRK",
                    size = "L",
                    availableSizes = listOf("S", "M", "L", "XL"),
                    similarItems = null,
                    imageUrl = "${listOfDomains[0]}img_shorts_white_alt_one.jpg"
            ),
            Product(
                    id = "9",
                    name = "ADIDAS kratke hlače AFC H SHO B",
                    description = "209.30 HRK",
                    size = "L",
                    availableSizes = listOf("S", "M", "L", "XL"),
                    similarItems = null,
                    imageUrl = "${listOfDomains[0]}img_shorts_white_alt_two.jpg"
            ),
            Product(
                    id = "10",
                    name = "ADIDAS shorts PARMA 16 SHO",
                    description = "119.00 HRK",
                    size = "L",
                    availableSizes = listOf("S", "L", "2XL"),
                    similarItems = null,
                    imageUrl = "${listOfDomains[0]}img_shorts_white_alt_three.jpg"
            )
    )

    private val products = listOf(
            Product(
                    id = "3000E280689000000001E86FBE0F",
                    name = "ADIDAS kratke hlače PARMA 16 SHO",
                    description = "119.00 HRK",
                    size = "L",
                    availableSizes = listOf("S", "M", "L", "XL"),
                    imageUrl = "${listOfDomains[0]}img_shorts_white.jpg",
                    similarItems = similarShorts
            ),
            Product(
                    id = "3000E280689000000001E86FC00C",
                    name = "ELLESSE majica kratki rukav GENTARIO",
                    description = "119.20 HRK",
                    size = "L",
                    availableSizes = listOf("M", "L", "XL"),
                    similarItems = similarShirts,
                    imageUrl = "${listOfDomains[0]}img_tshirt_white.jpg"
            ),
            Product(
                    id = "3000E280689000000001E86FC01C",
                    name = "CHAMPION čarape COTTON MEN",
                    description = "59.00 HRK",
                    size = "45/47",
                    availableSizes = listOf("36/38", "39/41", "42/44", "45/47"),
                    imageUrl = "${listOfDomains[0]}img_sport_socks.jpg"
            )
    )

    fun fetchTestProducts() = products

    fun fetchScannedIds() = scannedIds

    fun clearScannedIds() = scannedIds.clear()

    fun scanId(id: String) {
        //we only set unique ids
        if (!scannedIds.contains(id)) {
            scannedIds.add(id)
            SocketHandler.sendScannedProduct(products.single { it.id == id })
        }
    }

}