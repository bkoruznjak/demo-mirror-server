package hr.flowable.demomirror

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DemomirrorApplication

fun main(args: Array<String>) {
    System.getProperties()["server.port"] = 8080
    runApplication<DemomirrorApplication>(*args)
}
