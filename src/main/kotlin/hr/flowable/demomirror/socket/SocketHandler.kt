package hr.flowable.demomirror.socket

import com.google.gson.Gson
import hr.flowable.demomirror.model.Product
import org.springframework.web.socket.handler.TextWebSocketHandler
import org.springframework.web.socket.WebSocketSession
import org.springframework.web.socket.TextMessage
import java.io.IOException
import java.util.concurrent.CopyOnWriteArrayList

object SocketHandler : TextWebSocketHandler() {

    private val sessions = CopyOnWriteArrayList<WebSocketSession>()

    private val gson = Gson()

    @Throws(InterruptedException::class, IOException::class)
    override fun handleTextMessage(session: WebSocketSession, message: TextMessage) {
        for (webSocketSession in sessions) {
            webSocketSession.sendMessage(TextMessage("boo"))
        }
    }

    @Throws(Exception::class)
    override fun afterConnectionEstablished(session: WebSocketSession) {
        sessions.add(session)
        session.sendMessage(TextMessage("You are connected to the smart mirror demo server via websocket"))
    }

    fun sendIdScan(id: String) {
        for (webSocketSession in sessions) {
            webSocketSession.sendMessage(TextMessage(id))
        }
    }

    fun sendScannedProduct(product: Product) {
        for (webSocketSession in sessions) {
            val string = gson.toJson(product)
            val message = TextMessage(string)
            try {
                webSocketSession.sendMessage(message)
            } catch (ex: Exception) {
                webSocketSession.close()
                sessions.remove(webSocketSession)
                print("failed to send message due to $ex")
            }

        }
    }
}