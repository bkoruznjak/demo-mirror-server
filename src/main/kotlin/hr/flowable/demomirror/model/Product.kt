package hr.flowable.demomirror.model

data class Product(
        val id: String? = null,
        val name: String? = null,
        val description: String? = null,
        val imageUrl: String? = null,
        val size: String? = null,
        val availableSizes: List<String?>? = null,
        val similarItems: List<Product?>? = null
)
